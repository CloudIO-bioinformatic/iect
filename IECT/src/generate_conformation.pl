#!/usr/bin/perl

# Realiza el muestreo conformacional con el binario gp-pro
# Ejecucion: perl generate_conformation.pl $job_dir
# 
# Moises A. Rojas - Apr 2016
# Claudio I. Quevedo - March 2019

use strict;
use DBI;
#use lib('/home/IECT/lib');
#use conf;
use lib('/home/IECT/lib/conf.pm');
#my $db = DBI->connect("dbi:Pg:dbname='$conf::database';host=$conf::ip",$conf::user,$conf::pass);
my $db = DBI->connect("dbi:Pg:dbname='energy';host=localhost","iect2019","asd");

my $job_dir = $ARGV[0];

# Buscar trabajo en ejecucion
my $q = $db->prepare("SELECT * FROM job WHERE state=1");
$q->execute;
my @row = $q->fetchrow_array; 

#open(FILE1, ">gp-pro.conf");
#print FILE1 $row[17];
#close(FILE1);

# Monomero + Droga COMPLEJO
#my $input = $job_dir."entrada.pdb";
#open(FILE2, ">$input");
#print FILE2 $row[5]."\nTER\n".$row[6]."\nEND";
#close(FILE2);
my $input = $job_dir."entrada.pdb";
open(FILE2, ">$input");
print FILE2 $row[5]."\nTER\n".$row[6]."\nEND";
close(FILE2);
# Monomero
open(FILE3, ">".$job_dir."m1_monomer.pdb");
print FILE3 $row[5]."\nEND";
close(FILE3);

# Droga
open(FILE4, ">".$job_dir."m2_drug.pdb");
print FILE4 $row[6]."\nEND";
close(FILE4);

# ./gp-pro -i entrada -o salida -n sampling size y -a -b centros geometricos
my $output = $job_dir."complejo.pdb"; # complejo.log
print "\ninput \n";
print $input."\n";
print "output\n";
print $output."\n";
print "fila 9\n";
print $row[9]."\n";
print "fila 7\n";
print $row[7]."\n";
print "fila 8\n";
print $row[8]."\n";


system("./gp-pro -i $input -o $output -n ".$row[9]." -a $row[7] -b $row[8]");
#system("./gp-pro -i $input -o $output -n 1000 -a $row[7] -b $row[8]");

system("rm -f $input");
#system("rm -f gp-pro.conf");
$q->finish;

# Si existe un error al crear las N conformaciones, cambiar estado del trabajo a erroneo
my $log = $job_dir."complejo.log";
my $num_files = `cat $log|grep '\n'|wc -l`;
if ($row[9] != $num_files) {
	print ("no genera los pdbs\n");
	my $qt = $db->prepare("UPDATE job SET state=3,log='Error creating conformations with gp-pro.\nMissing atoms in system.' WHERE state=1");
	$qt->execute;
	$qt->finish;
}

$db->disconnect;
