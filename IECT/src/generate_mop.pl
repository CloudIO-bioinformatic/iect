#!/usr/bin/perl

# Genera archivos para MOPAC a partir de los PDB del muestreo.
# Ejecucion: perl generate_mop.pl $job_dir
# 
# Moises A. Rojas - Apr 2016
# Claudio I. Quevedo - March 2019

use strict;
use DBI;
#use lib('/home/IECT/lib');
#use conf;
use POSIX qw(strftime);
use lib('/home/IECT/lib/conf.pm');
#my $db = DBI->connect("dbi:Pg:dbname='$conf::database';host=$conf::ip",$conf::user,$conf::pass);
my $db = DBI->connect("dbi:Pg:dbname='energy';host=localhost","iect2019","asd");
# Crear subdir mop/, para los ficheros MOP, dentro del dir principal
my $dir = $ARGV[0];
my $dir_mop = $dir."mop/";
system("mkdir $dir_mop");

### Generar ficheros .mop. Imprimir 1er estado ###
my $q1 = $db->prepare("UPDATE machine SET msg='Transforming PDB to MOP input files' WHERE name='fabian'");
$q1->execute;
$q1->finish;
sleep(2);
print ("aqui vamos bien 'Transforming PDB to MOP input files'\n ");
# Listar todos los archivos PDB
my $lista_pdb = `ls $dir`;
my @pdbs = split("\n", $lista_pdb);

foreach my $p (@pdbs) {
	# Quitar extension .pdb y agregar ruta
	$p = $dir.substr($p, 0, index($p, "."));
	print ($p."\n");
	# Nombre actual
	my @arr = split('/', $p);
	my $name = $arr[scalar(@arr)-1];
	print("name = ".$name."\n");
	print ("iniciando babel\n");
	# Monomero (m1_monomer.mop) y droga (m2_drug.mop)
	if (index($p, $dir."m1_") == 0 or index($p, $dir."m2_") == 0) {
		print("babel m1 y m2\n");
		system("babel -ipdb ".$p.".pdb -omop ".$dir_mop.$name.".mop");
	}
	# Complejos (c_complejo_00000X.mop)
	elsif (index($p, $dir."complejo_") == 0) {
		print("babel complejo\n");
		system("babel -ipdb ".$p.".pdb -omop ".$dir_mop."c_".$name.".mop");
	}
}

### Agregar keywords para MOPAC2016. Imprimir 2do estado ###
my $q2 = $db->prepare("UPDATE machine SET msg='Adding MOPAC2016 keywords in MOP files' WHERE name='fabian'");
$q2->execute;
$q2->finish;
sleep(2);
# Obtener la lista de argumentos del trabajo
my $q = $db->prepare("SELECT parametric_method,bond_system,monomer_charge,drug_charge,complex_charge,monomer_multiplicity,drug_multiplicity,complex_multiplicity FROM job WHERE state=1");
$q->execute;
my @row = $q->fetchrow_array;

# Crear cabecera
my $key_scf = $row[0]." 1SCF"; # PULAY CAMP
my $key_opt = "MMOK ITRY=200"; # peptide bonds (ignore if not), limit iterations in SCF
if ($row[1] ne "-") {
	$key_opt .= " BONDS";
}
my $key_aux = "T=60.0 THREADS=1"; # Performance options: time limited, multi-threading disabled

# Capturar cargas y multiplicidades
my $chm_m = "CHARGE=".$row[2]." ".$row[5];
my $chm_d = "CHARGE=".$row[3]." ".$row[6];
my $chm_c = "CHARGE=".$row[4]." ".$row[7];

$q->finish;

# Asignar keywords a todos los archivos mop
my $lista_mop = `ls $dir_mop`;
my @mops = split('\n', $lista_mop);

foreach my $m (@mops) {
	# Quitar extension .mop y agregar ruta
	my $m = $dir_mop.substr($m, 0, index($m, "."));
	# Complejos
	if (index($m, $dir_mop."c_") == 0) {
		my $m_key = `cat $m.mop|sed 's/PUT KEYWORDS HERE/$key_scf $key_opt $chm_c $key_aux/g'`;
		open OUT, ">$m.mop";
		print OUT $m_key;
		close(OUT);
	}
	# Monomero
	elsif (index($m, $dir_mop."m1_") == 0) {
		my $m_key = `cat $m.mop|sed 's/PUT KEYWORDS HERE/$key_scf $key_opt $chm_m $key_aux/g'`;
		open OUT, ">$m.mop";
		print OUT $m_key;
		close(OUT);
	}
	# Droga
	elsif (index($m, $dir_mop."m2_") == 0) {
		my $m_key = `cat $m.mop|sed 's/PUT KEYWORDS HERE/$key_scf $key_opt $chm_d $key_aux/g'`;
		open OUT, ">$m.mop";
		print OUT $m_key;
		close(OUT);
	}
}

$db->disconnect;
