#!/usr/bin/perl

# Distribuye los calculos de MOPAC2016 sobre N procesadores
# Ejecucion: perl serialize_mopac.pl $job_dir
# 
# Moises A. Rojas - Apr 2016
# Claudio I. Quevedo - March 2019

use strict;
use DBI;
#use lib('/home/IECT/lib');
#use conf;
use POSIX qw(strftime);
use lib('/home/IECT/lib/conf.pm');
#my $db = DBI->connect("dbi:Pg:dbname='$conf::database';host=$conf::ip",$conf::user,$conf::pass);
my $db = DBI->connect("dbi:Pg:dbname='energy';host=localhost","iect2019","asd");
my $dir = $ARGV[0];

# Consultar el numero de procesadores
my $q = $db->prepare("SELECT processors_number FROM job WHERE state=1");
$q->execute;
my $n_proc = $q->fetchrow_array;
$q->finish;

# Listar ficheros MOP (subdir mop/)
my $dir_mop = $dir."mop/";
my $lista_mop = `ls $dir_mop|grep 'mop'`;
my @mops = split('\n', $lista_mop);

my $total = scalar(@mops);
my %queue = ();
my $p = 1;

# Crear una cola de trabajos igual para cada procesador
for (my $i=0; $i<$total; $i++) {
	if ($p == $n_proc) {
		$queue{$p} .= pop(@mops).",";
		$p=1;
	}
	else {
		$queue{$p} .= pop(@mops).",";
		$p++;
	}
}

### Chequear sintaxis de archivos mop. Imprimir 1er estado ###
my $q1 = $db->prepare("UPDATE machine SET msg='Checking keywords syntax in MOP files' WHERE name='fabian'");
$q1->execute;
$q1->finish;
sleep(2);
# Procesar un complejo, monomero y droga
system("/opt/mopac/MOPAC2016.exe ".$dir_mop."c_complejo_000000.mop");
system("/opt/mopac/MOPAC2016.exe ".$dir_mop."m1_monomer.mop");
system("/opt/mopac/MOPAC2016.exe ".$dir_mop."m2_drug.mop");

# Guardar ficheros OUT de salida
my @outs;
push(@outs, $dir_mop."c_complejo_000000.out");
push(@outs, $dir_mop."m1_monomer.out");
push(@outs, $dir_mop."m2_drug.out");

# Buscar errores
my $check = 0;
foreach my $o (@outs) {
	my $cmd = `grep -c 'TOTAL ENERGY' $o`; # -c devuelve 1 si esta
	if ($cmd == 1) {
		$check++;
	}
}

### Lanzar instancias de MOPAC. Imprimir 2do estado ###
my $q2 = $db->prepare("UPDATE machine SET msg='Performing optimizations with MOPAC2016' WHERE name='fabian'");
$q2->execute;
$q2->finish;
sleep(2);
if ($check == 3) {
	# Ordenar hash y extraer max = $n_proc
	my @procs = sort {$a<$b} keys %queue;
	print ("\n###################################################proc 0 = ".$procs[0]."\n");
	for (my $j=1; $j<=$procs[0]; $j++) {
		print("\n============================JOTA!!!! = ".$j."\n");
		chop($queue{$j}); # Borra ultima coma ','
		open(FILE, ">proc$j");
		print FILE $queue{$j};
		close(FILE);
		system("perl generate_out.pl $dir $j &");
	}
}
else {
	# Notificar un error y cambiar trabajo actual a fallido
	my $qt = $db->prepare("UPDATE job SET state=3,log='Error in optimizations with MOPAC2016.\nCheck all charges and multiplicities.' WHERE state=1");
	$qt->execute;
	$qt->finish;
	sleep(2);
}

$db->disconnect;
