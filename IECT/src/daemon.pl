#!/usr/bin/perl

# Programa principal del software IECT.
# Ejecucion: nohup perl daemon.pl > /dev/null &
# 
# Moises A. Rojas - Apr 2016
# Claudio I. Quevedo - March 2019
print "ingreso al daemon.pl\n";
use strict;
use DBI;
use lib('/home/IECT/lib/conf.pm');
#use conf;
use POSIX qw(strftime);

my $db = DBI->connect("dbi:Pg:dbname='energy';host=localhost","iect2019","asd");
print "va a reset machine \n";
# Limpiar estado de maquina
system("perl reset_machine.pl");

# Global vars
my $job_id = 0;
my $job_running = 0;
my $machine_status = -1;
my $n_proc;
my $job_dir;
my $job_status;
print "va al while\n";
while (1) {
	print "entro al while\n";
	# Comprobar que no existan trabajos en ejecucion
	if ($job_running == 0) {
		print "running igual a 0\n";
		# Comprobar si existe al menos un trabajo en espera para ser procesado
		my $q = $db->prepare("SELECT * FROM job WHERE state=0");
		$q->execute;
		my $n = $q->rows;
		$q->finish;
		
		if ($n > 0) {
			
			print("n mayor que 0\n");
			# Consulta que obtiene los datos de los trabajos en estado = 0 (en espera)
			my $q1 = $db->prepare("SELECT * FROM job WHERE state=0 ORDER BY id_job ASC");
			$q1->execute;
			# Obtener el id del trabajo mas antiguo (primer elemento, menor id)
			my @row1 = $q1->fetchrow_array;
			
			$job_id = $row1[0];
			$q1->finish;
			# Obtener el estado de la maquina (aqui debe ser 0=idle)
			my $q2 = $db->prepare("SELECT state FROM machine WHERE name='fabian'");
			$q2->execute;
			$machine_status = $q2->fetchrow_array;
			$q2->finish;
			# Obtener el numero de procesadores asignados
			my $q3 = $db->prepare("SELECT processors_number FROM job WHERE id_job=$job_id");
			$q3->execute;
			$n_proc = $q3->fetchrow_array;
			$q3->finish;
			
			# Actualizar hora de inicio del trabajo
			my $start = strftime "%F %X", localtime;
			my $q4 = $db->prepare("UPDATE job SET job_date='$start' WHERE id_job=$job_id");
			$q4->execute;
			$q4->finish;
			
			# Flag para trabajo en ejecucion
			$job_running = 1;
		}
		else {
			# Si no existe ningun trabajo en espera, setear variable a 0
			$job_id = 0;
		}
	}
	
	# Proceder si el estado de la maquina es 0 (desocupada), que exista un identificador
	# asociado a un nuevo trabajo y que la bandera este en modo de ejecucion
	if ($machine_status == 0 and $job_id != 0 and $job_running == 1) {
		# Cambiar estado de la maquina a 1 (ocupada) y setear bandera a 1
		my $qm1 = $db->prepare("UPDATE machine SET state=1 WHERE name='fabian'");
		$qm1->execute;
		$qm1->finish;
		$machine_status = 1;
		
		# Cambiar estado del trabajo a 1 (en ejecucion)
		my $q5 = $db->prepare("UPDATE job SET state=1 WHERE id_job=$job_id");
		$q5->execute;
		$q5->finish;
		
		# Consultar el nombre del trabajo actual
		my $q6 = $db->prepare("SELECT process_name FROM job WHERE id_job=$job_id");
		$q6->execute;
		my $job_name = $q6->fetchrow_array;
		$q6->finish;
		
		# Crear directorio con formato 'nombre_id'. Ejemplo: trabajo_17/
		my $folder = $job_name."_".$job_id;
		$job_dir = "/home/fabian/IECT/src/jobs/$folder/";
		system("mkdir $job_dir");
		system("chmod -R 777 $job_dir");
		
		### PASO 1 ###
		# Generar las N conformaciones con gp-pro, en formato PDB
		my $qm2 = $db->prepare("UPDATE machine SET msg='Creating conformations with gp-pro' WHERE name='fabian'");
		$qm2->execute;
		$qm2->finish;
		sleep(2);
		print "nos fuimos a generate_conformation.pl";		
		system("perl generate_conformation.pl $job_dir");
		
		# Obtener el estado del trabajo en ejecucion
		my $q7 = $db->prepare("SELECT state FROM job WHERE id_job=$job_id");
		$q7->execute;
		$job_status = $q7->fetchrow_array; # Debe ser != 0
		$q7->finish;
		
		# Continuar solo si se han generado las N conformaciones de forma correcta
		if ($job_status != 3) {
			print "entra al job status =3\n";
			### PASO 2 ###
			# Generar los archivos MOP a partir de los PDBs del muestreo
			system("perl generate_mop.pl $job_dir");
			
			### PASO 3 ###
			# Lanzar instancias de MOPAC2016 para procesar los archivos MOP
			system("perl serialize_mopac.pl $job_dir");
		}
	}
	
	# Se comprueba si existe un identificador asociado a un trabajo
	if ($job_id != 0) {
		my $q8 = $db->prepare("SELECT state FROM job WHERE id_job=$job_id");
		$q8->execute;
		$job_status = $q8->fetchrow_array;
		$q8->finish;
	}
	
	# Continuar si los calculos con MOPAC han finalizado correctamente para el trabajo actual
	if ($job_status != 3 and $job_id != 0) {
		# Consultar el estado de cada procesador, por separado
		my $flag = 0;
		for (my $i=1; $i<=$n_proc; $i++) {
			my $qm3 = $db->prepare("SELECT p$i FROM machine WHERE name='fabian'");
			$qm3->execute;
			my $p = $qm3->fetchrow_array;
			$flag += $p;
		}
		
		# Verificar si todos los procesadores han terminado las instancias de MOPAC
		if ($flag == ($n_proc*100)) {
			### PASO 4 ###
			# Procesar las salidas de MOPAC y generar los resultados
			my $qm4 = $db->prepare("UPDATE machine SET msg='Analyzing data and generating results' WHERE name='fabian'");
			$qm4->execute;
			$qm4->finish;
			sleep(2);
			# Mover resultados a subdirs arc/ y mop/ segun formato de salida
			system("perl move_outs.pl $job_dir");
			
			# Realizar la curacion de datos y guardar info en la BD
			system("perl generate_result.pl $job_dir");
			
			# Actualizar el estado del trabajo a 2 = exitoso, y almacenar la hora de termino
			my $stop = strftime "%F %X", localtime;
			my $q9 = $db->prepare("UPDATE job SET state=2,end_date='$stop',log='Job successfully done.' WHERE id_job=$job_id");
			$q9->execute;
			$q9->finish;
			
			# Resetear el estado de la maquina
			system("perl reset_machine.pl");
			
			# Borrar directorio y restaurar flags
			system("rm -rf $job_dir");
			$job_running = 0;
			$machine_status = 0;
			#exit();
		}
	}
	# Estado del trabajo = 3 erroneo
	else {
		# Descartar trabajo y almacenar la hora de termino
		my $stop = strftime "%F %X", localtime;
		my $q10 = $db->prepare("UPDATE job SET state=3,end_date='$stop' WHERE id_job=$job_id");
		$q10->execute;
		$q10->finish;
		
		# Resetear estado de la maquina
		system("perl reset_machine.pl");
		
		# Borrar directorio y restaurar flags
		system("rm -rf $job_dir");
		$job_running = 0;
		$machine_status = 0;
	}
	
	sleep(2);
}

$db->disconnect;
