#!/usr/bin/perl

# Procesar trabajo para generar resultados.
# Ejecucion: perl generate_result.pl $job_dir
# 
# Moises A. Rojas - Apr 2016
# Claudio I. Quevedo - March 2019

use DBI;
#use lib('/home/IECT/lib');
#use conf;
use POSIX qw(strftime);
use lib('/home/IECT/lib/conf.pm');
#my $db = DBI->connect("dbi:Pg:dbname='$conf::database';host=$conf::ip",$conf::user,$conf::pass);
my $db = DBI->connect("dbi:Pg:dbname='energy';host=localhost","iect2019","asd");
# Solo hay un trabajo en ejecucion (estado=1)
my $q = $db->prepare("SELECT id_job,outputs_number,evaluation_algorithm,interaction_energy_type FROM job WHERE state=1");
$q->execute;
my $num_results;
my $i = 1;
my $id_job;
while (my @row = $q->fetchrow_array) { 
	$id_job = $row[0];
	$num_results = $row[1];
	$A[0] = $row[2];
	$A[1] = $row[3];
	$i++;
}

our %hash_pdb;
our $prom_s;
our $prom_m;
our $min_s;
our $max_s;
our $min_m;
our $max_m;
my $selectEnergy;
our @SELECTED_PDB;
our @ACHIVO;
our @EN_ORIGEN;
our @EN_OPTIMA;
our $k = 0;
our $CONT_PDB = 0;  
our $directory = $ARGV[0];
my @ORDER;
my $TYPE = 1;

if ($A[1] eq "F") {
	$TYPE = 1; # Entalpia de formacion
}
else {
	if ($A[1] eq "T") {
		$TYPE = 2; # Energia total
	}
}

# Evaluacion Promedio Simple y/o Metropolis
if ($A[0] eq "A") {
	$ORDER[0] = 'M';
	$ORDER[1] = 'S';
}
else {
	if ($A[0] eq "M") {
		$ORDER[0] = 'M';
		$ORDER[1] = ' ';
	}
	if ($A[0] eq "S") {
		$ORDER[0] = ' ';
		$ORDER[1] = 'S';
	}
}

# Abrir subdir out/ para los ficheros .out
&open_dir($directory."out/", ".out");

my @file;
my $comp;
my $mole1;
my $mole2;
my $i;
my @name_mole;
my @pdb;

for ($i=0; $i<$k; $i++) { 
	@AUX = split("_", $ARCHIVO[$i]);
	# Monomero
	if ($AUX[0] =~/m1/) {
		$mole1 = $ARCHIVO[$i];
		$name_mole[0] = $AUX[1];
	}
	# Droga
	if ($AUX[0] =~/m2/) {
		$mole2 = $ARCHIVO[$i];
		$name_mole[1] = $AUX[1];
	}
}

######## SEGUIR #####
for ($i=0;$i<$k;$i++) {   
	  $file=$ARCHIVO[$i];
	 
	  @file=split("_",$file);
	  
	 
	  if($file[0]=~/c/)
		{
		 $name_mole[2]=$file[1];
		 $comp=$file ;
		 #@AUX=split("out",$file[2]);
         #$pdb[$CONT_PDB]=$AUX[0];
		 $pdb[$CONT_PDB]=$file[2];
##################################################################################
#										##
#		COMPLEX,MONOMER & DRUG					        ##
# Son FileHandle, de los .out del complejo,monomero y droga, respectivamente  	##
#										##
##################################################################################

		@fname=($comp,$mole1,$mole2);

#################ABRIR ARCHIVOS##################
		eval{
		open COMPLEX,$directory."out/".$fname[0] or die $!;  
		open MONOMER,$directory."out/".$fname[1] or die $!;     
		open DRUG,$directory."out/".$fname[2] or die $!; 
		  };
			if($@)
			  {
				print " OopsFalla al abrir fichero .out $@";
			  }
  ####FIN ABRIR ARCHIVOS ######################

      
     $this_pdb=$pdb[$CONT_PDB];
    
    $this_pdb=substr($this_pdb,0,(length($this_pdb)-4));
    $this_pdb=$this_pdb.".pdb";
    
     #PARA Heat of formation
	 my $C= &DHformation("COMPLEX") if($TYPE==1);
     my $m1= &DHformation("MONOMER") if($TYPE==1);
     my $m2= &DHformation("DRUG") if($TYPE==1);
     #PARA energia total 
     if($TYPE==2){
      $C=  &TotalEnergy("COMPLEX") if($TYPE==2);
      $m1= &TotalEnergy("MONOMER") if($TYPE==2);
      $m2= &TotalEnergy("DRUG") if($TYPE==2);
				}
     $num=$CONT_PDB+1;
     
     #############################################################
     #  Descomente para imprimir la lectura de .out por pantalla #
     #############################################################
		$nombre_p=$name_mole[2]."_".$this_pdb;
		$EN_ORIGEN[$CONT_PDB]=&Ecuacion($C,$m1,$m2);
		
		$l=$l.$num."#".$nombre_p."&".$EN_ORIGEN[$CONT_PDB]."kcal/mol\n";

		#print $num."#".$nombre_p."&" if($ORDER[1] eq 'S');
     		#print  "comp:" .$name_mole[2]." : ". $C."              ";
		# print  "m1 ".$name_mole[0]." : ".$m1 ."                \n";
		# print  "m2 ".$name_mole[1]." : ". $m2."                \n";
		#print $EN_ORIGEN[$CONT_PDB]."kcal/mol               \n" if($ORDER[1] eq 'S');
		#  print "_________________________________________________\n";
		
		$hash_pdb {$EN_ORIGEN[$CONT_PDB]} = $nombre_p;#INICIALIZA EL HASH, asignando a cada key de energia un nombre de pdb


$CONT_PDB++;
close COMPLEX;
close MONOMER;
	close DRUG;
       
       
       
        }

	}
	
	

	
	 $i=0;
	 
	 &Metropolis();
	 
	 &Prom_Max_Min();
	
	 
	
	 my $j;
	 
	 
	 	$k = 0;
	    &open_dir($directory,".pdb");#abrir directorio para los .pdb
	 @lineas;
	 $l2="";
	
	 foreach $selectEnergy (@EN_OPTIMA)
	 
	 {
		 #Procede a calcular el promedio de la energia obtenida seleccionada.#
		 #Procede a generar una lista y pegarla en un archivo .txt con los datos del pdb asociado y la energia obtenida
		 #de aquellas más estables.
		 $num=$i+1;
		 $j=$SELECTED_PDB[$i];
		 $this_pdb=$pdb[$j];
	     $this_pdb=substr($this_pdb,0,(length($this_pdb)-4));
		 $this_pdb2=$this_pdb.".pdb";
	  if($ORDER[0] eq 'M')
		 {
          foreach $file (@ARCHIVO)  
          # Las siguientes lineas abren los .pdb del directorio  origen, y seleccionan unicamente
          #aquellos cuya Energia de interacción ha sido evaluada e iterpretada como MÁS estable.
			{	
				  @split=split("_",$file);
				 #print $split[0]."\n";
				  if($split[0] eq $name_mole[2])
				  {  #print $split[1]. "\n";
				      
					  if($this_pdb2 eq $split[1])
						{	
							$PDB_M[$i]=$file;	
					    }
				  }				  			  				
				}	
	 ########################################################################
     #  Descomente para imprimir por pantalla el archivo con las n evaluadas#
     ########################################################################
		 #print $num."-".$this_pdb."&";
	     #print $selectEnergy. "\n";
		
		 $l2=$l2.$num."#".$name_mole[2]."_".$this_pdb."&".$selectEnergy." \n" ;	
			}
	$i++;
     
   }
    
     my $q2 = $db->prepare("INSERT INTO result
      (id_job,algorithm,energy_average,min_energy,max_energy,all_pdb)
	 VALUES ('$id_job','S','$prom_s','$min_s','$max_s','$l')");
	 $q2->execute;
    
      my $q3 = $db->prepare("INSERT INTO result (id_job,algorithm,energy_average,min_energy,max_energy,all_pdb)
	 VALUES ('$id_job','M','$prom_m','$min_m','$max_m','$l2')");
	 $q3->execute;

	 #print "_________________Terminado ::CON EXITO!!________________\n";
	
	
	
	   insercion(\@EN_ORIGEN) if($ORDER[1] eq 'S');
	   insercion(\@EN_OPTIMA) if($ORDER[0] eq 'M');
	 $i=0;
	 $in,$cpy;
	 $flag=0;
	 $j=0;
	   foreach $in( @EN_OPTIMA)
	   {    
		   if($in>=$prom_m)
			 {
			$flag=1;
		     }
		      if($flag==0)
		    {
			  
			  $i++;
			}
		  $j++;
	   }
	 #  $num_results=10;
	   
	   $cpy=$i;   
	   
	  
	   my $left,$right;#contendría los valores por encima y por debajo del proedio. exigidos
	     
	   $left=($num_results/2);
	   $right=$left;
	   
	   $inits=($cpy+1)-$left;
	   
	for($i=$inits;$i<=$cpy;$i++) {
	   
		print "\nEN OPTIMA M1: ".$EN_OPTIMA[$i]."\t";
		print "\nhas_pdb: ".$hash_pdb{$EN_OPTIMA[$i]}."\n";
		#print("EN_ORIGEN M1= ".$EN_OPTIMA[$i]);
		$value_pdb=`cat $directory$hash_pdb{$EN_OPTIMA[$i]}`;
		my $q4 = $db->prepare("INSERT INTO pdb_100 (algorithm,pdb_name,energy,content)
		VALUES ('M','$hash_pdb{$EN_OPTIMA[$i]}',$EN_OPTIMA[$i],'$value_pdb')");
		$q4->execute;
		$q4->finish;
	}

	$inits=$cpy+1; 
	$cpy=($cpy+$right);

	#print "sobre el promedio:\n";

	#### NO EJECUTAR SI NO SE HA SELECCIONADO METROPOLIS ###
	for($i=$inits;$i<$cpy;$i++) {
		print("EN_OPTIMA M2= ".$EN_OPTIMA[$i]);
		$value_pdb=`cat $directory$hash_pdb{$EN_OPTIMA[$i]}`;
		my $q4 = $db->prepare("INSERT INTO pdb_100 (algorithm,pdb_name,energy,content)
		VALUES ('M','$hash_pdb{$EN_OPTIMA[$i]}',$EN_OPTIMA[$i],'$value_pdb')");
		$q4->execute;
		$q4->finish;	
	}
	  
	  
	  
	 ##encuentra los mejores en simple:
	 
	$i=0;
	$in,$cpy;
	$flag=0;
	
	foreach $in( @EN_ORIGEN) {
		if($in>=$prom_s) {
			$flag=1;
		}
		if($flag==0) {
			$i++;
		}
	}
	  
	#print "el prom es:".$prom_s."\n";
	$cpy=$i;	   
	# $num_results=10;
	$total=20;    
	$left=($num_results/2);

	$right=$left;
	$inits=($cpy+1)-$left;
	
	for($i=$inits;$i<=$cpy;$i++) {
		print("EN_ORIGEN S1= ".$EN_ORIGEN[$i]);
		#print "\nEN OPTIMA S1: ".$EN_OPTIMA[$i]."\t";
		#print "\nhas_pdb: ".$hash_pdb{$EN_OPTIMA[$i]}."\n";
		
		$value_pdb=`cat $directory$hash_pdb{$EN_ORIGEN[$i]}`;
		my $q4 = $db->prepare("INSERT INTO pdb_100 (algorithm,pdb_name,energy,content)
		VALUES ('S','$hash_pdb{$EN_ORIGEN[$i]}',$EN_ORIGEN[$i],'$value_pdb')");
		$q4->execute;
		$q4->finish;
	}


	$inits=$cpy+1; 
	$cpy=($cpy+$right);
	#$contador=0;
	# print "sobre el promedio:\n";
	my $query2 = $db->prepare("UPDATE machine SET msg='Ending the program' WHERE name='fabian'");
	$query2->execute;
	$query2->finish;
	sleep(2);
	for($i=$inits;$i<$cpy;$i++) {
	       # $contador++;
		print("EN_ORIGEN S2= ".$EN_ORIGEN[$i]);
		#print "\nEN OPTIMA S2: ".$EN_OPTIMA[$i]."\t";
		#print "\nhas_pdb: ".$hash_pdb{$EN_OPTIMA[$i]}."\n";
		print("id_job: ".$id_job."\n");
		$value_pdb=`cat $directory$hash_pdb{$EN_ORIGEN[$i]}`;
		my $q4 = $db->prepare("INSERT INTO pdb_100 (algorithm,pdb_name,energy,content)
		VALUES ('S','$hash_pdb{$EN_ORIGEN[$i]}',$EN_ORIGEN[$i],'$value_pdb')");
		$q4->execute;	
		$q4->finish;  

	}
     # print "Cantidad:  ".$contador;
	 #print "el prom en SIMPLE: ".$prom_s." el min: ".$min_s." y max ".$max_s."\n";
	 # print "el prom en Metropolis: ".$prom_m." el min: ".$min_m." y max ".$max_m."\n";
	 
	
	 
	 
	 
	 
	 
##########################################FIN MAIN########################################################################
############################################################################################################################	
	
############################################################################################################################
#################INICIO DE FUNCIONES &######################################################################################
############################################################################################################################
		
	 ###################################
     #        Funcion abrir directorios#
     ################################### 
##Recorrer el directorio donde encuentro los .out  o .pdb segun el parametro ingresado##
sub open_dir
	{
		my ($path) = ($_[0]);
		
		my $ext=($_[1]);
		opendir(DIR, $path) or die $!; #se abre el directorio
		my @files = grep(!/^\./,readdir(DIR));

		closedir(DIR);
		my $dfile;
		foreach $dfile (@files)
			{
				my $tfile=$dfile;
				$dfile = $path.'/'.$dfile; #path absoluto del fichero o directorio
			
				next unless( -f $dfile or -d $dfile ); #se rechazan pipes, links, etc ..
				if( -d $dfile)
				   {
					open_dir($dfile,$hash);
				   }
				else{
					
					if($tfile=~$ext)
						{  
							$ARCHIVO[$k]=$tfile ;
							$k++;
						}
					}
		
			}
		}############################End funcion #######################
		
	####################################################################
	#                 &Ecuacion resuleve Energia de Interacción        #
	####################################################################
	sub Ecuacion 
	{

		my $complex= shift;
		my $m1= shift;
		my $m2= shift;	
		my $DE=$complex-($m1+$m2);
		return $DE;
	}############################End funcion #######################
		
	#####################################################################################
	#                 &DHformacion obtiene calores de formacion desde fichero .out      #
	#####################################################################################
	sub DHformation
	{
			$Handle= shift;#Handle,  tendrá el nombre del FileHandle que se le envie a la funcion.
			my $linea;
			my $q='FINAL HEAT ';
		
			my $DEf;
			my @array;
				while ( $linea = <$Handle>) 
			{
	  		 	 chomp($linea);
					if($linea=~/$q/)
					{ 	
						@array=split(" ",$linea);
		
						$DEf=$array[5];
	   				
					}
		
			}
		  return $array[5];
		
	}############################End funcion #######################
	
		sub TotalEnergy
	{
		$Handle= shift;#Handle,  tendrá el nombre del FileHandle que se le envie a la funcion.
			my $linea;
			my $q='TOTAL ENERGY';
		
			my $TE;
			my @array;
				while ( $linea = <$Handle>) 
			{
	  		 	 chomp($linea);
					if($linea=~/$q/)
					{ 	
						@array=split(" ",$linea);
		                
						$TE=$array[3];
	   				    	$TE = $TE*23.06054;
						#print "EV= ".$array[3]." calor: ".$TE."\n";
					}
		
			}
		  return $TE;
		
	}############################End funcion #######################
	
	sub Metropolis 
	{     my $T=298; #298 K
	      my $power=10**-23; # e -23
		  my $Kb=(1.3806504*$power);
		  my $value=$EN_ORIGEN[0];
		  my $iterate=0;
		  my $energy;
		  my $randE;
		  my $numer;
		  my $denom;
		  my $cocient;
		  my $count=0;
		  my $E;
		 foreach $energy(@EN_ORIGEN)	 
		 {  
			 $E=$value-$energy;
			 if($E<=$value)
				{ 
				  $SELECTED_PDB[$iterate]=$count;
			      $EN_OPTIMA[$iterate]=$energy;		
				  $iterate++;	
				  $value=$energy;
				}
				else #Evaluar con respecto a constante de Boltzmann
				   {
						#Si evalua y es da un resultado >=Z , se queda en el sistema
						# $value=$energy;  
						#sino, se rechaza y se margina del sistema. y se continúa evaluando.
						
						$z=$randE=rand(rand);
						$denom=$Kb*$T;
						
						
						
						$cocient=((-$E)/$denom);
						$numer=exp($cocient);
						
						if($numer>=$z)
						{
							
							 $SELECTED_PDB[$iterate]=$count;
							 $EN_OPTIMA[$iterate]=$energy;	
							 $value=$energy;
							 $iterate++;
						}	
									
				   	 
			 
					}
	   	     	$count++; 
	    }
	}
	
	sub Prom_Max_Min
	{   my $prom=0;
		my $sum=0;
		my $n=0;
		
	    $max_m=$EN_OPTIMA[0];
	    $min_m=$EN_OPTIMA[0];
	    $max_s=$EN_ORIGEN[0];
	    $min_s=$EN_ORIGEN[0];
	    
		foreach $sum(@EN_ORIGEN)	
		{   
			$max_s=$sum if($max_s<=$sum);
			$min_s=$sum if($min_s>=$sum);
			
			
			
			$prom=$sum+$prom;
			$n++;
		}
		  
		  $prom_s=$prom/$n;
		  #print $prom_s." entre ".$n."\n";
		  
		  $n=0;
		  $prom=0;
		  $sum=0;
		  
		  foreach $sum (@EN_OPTIMA)	
		{
			$max_m=$sum if($max_m<=$sum);
			$min_m=$sum if($min_m>=$sum);
			$prom=$sum+$prom;
			$n++;
		}
		 $prom_m=$prom/$n;
		 #print $prom_m." entre ".$n."\n";
	   }
  
    sub insercion {
    my $array_ref = shift;                              # el primer argumento es una ref. a un array
 
    for my $i (1 .. $#$array_ref) {                     # para todos los índices
 
        my $j = $i - 1;                                 # índice anterior
        my $x = $array_ref->[$i];                       # elemento a comparar
 
        next if $array_ref->[$j] <= $x;                 # salida rápida si ya están ordenados
 
        do {
            $j--;                                       # buscamos
        } while ($j >= 0  and  $array_ref->[$j] > $x);
 
        splice(@$array_ref, $j+1, 0, splice(@$array_ref, $i, 1));   # ¡extracción e inserción!
    }
    
}
  
