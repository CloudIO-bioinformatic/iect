#!/usr/bin/perl

# Script que mueve los archivos de salida generados por MOPAC2016.
# Ejecucion: perl move_outs.pl $job_dir
# 
# Moises A. Rojas - Apr 2016
# Claudio I. Quevedo - March 2019

use strict;

my $dir = $ARGV[0];
my $dir_mop = $dir."mop/"; # .mop obtenidos de OpenBabel
my $dir_out = $dir."out/"; # .out obtenidos de MOPAC2016
my $dir_arc = $dir."arc/"; # .arc obtenidos de MOPAC2016

# Crear subcarpetas dentro del directorio principal
system("mkdir $dir_out");
system("mkdir $dir_arc");

# Mover archivos .arc y .out
# find ../jobs/TEST-7_295/mop/ -name *.out -exec mv {} ../jobs/TEST-7_295/out/ \;
system("find $dir_mop -name *.out -exec mv {} $dir_out \\;");
system("find $dir_mop -name *.arc -exec mv {} $dir_arc \\;");
